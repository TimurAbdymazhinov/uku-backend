from rest_framework import serializers

from apps.system.models import (
    PrivacyPolicy, TermsOfUse, FAQ, Contact, ContactPhone
)


class PrivacyPolicySerializer(serializers.ModelSerializer):
    class Meta:
        model = PrivacyPolicy
        fields = ('id', 'title', 'description',)


class TermsOfUseSerializer(serializers.ModelSerializer):
    class Meta:
        model = TermsOfUse
        fields = ('id', 'title', 'description',)


class FAQSerializer(serializers.ModelSerializer):
    class Meta:
        model = FAQ
        fields = ('id', 'question', 'answer',)


class ContactPhoneSerializer(serializers.ModelSerializer):

    class Meta:
        model = ContactPhone
        fields = ('id', 'phone')


class ContactSerializer(serializers.ModelSerializer):
    phone_numbers = ContactPhoneSerializer(many=True)

    class Meta:
        model = Contact
        fields = (
            'id', 'title', 'description', 'image', 'telegram', 'facebook',
            'instagram', 'address', 'phone_numbers'
        )

from django.db import models

from ckeditor_uploader.fields import RichTextUploadingField
from solo.models import SingletonModel

from common.utils import generate_filename


class StaticPage(SingletonModel):
    title = models.CharField(
        max_length=255, null=True, verbose_name='Название'
    )
    description = RichTextUploadingField(verbose_name='Описание')

    def __str__(self):
        return self.title

    class Meta:
        abstract = True


class PrivacyPolicy(StaticPage):
    class Meta:
        verbose_name = 'Политика конфиденциальности'


class TermsOfUse(StaticPage):
    class Meta:
        verbose_name = 'Пользовательское соглашение'


class FAQ(models.Model):
    question = models.CharField(
        max_length=255, null=True, verbose_name='Вопрос'
    )
    answer = models.CharField(
        max_length=255, null=True, verbose_name='Ответ'
    )
    position = models.PositiveIntegerField(
        default=0, blank=False, null=False, verbose_name='№',
    )

    class Meta:
        ordering = ['position']
        verbose_name = 'Часто задаваемый вопрос'
        verbose_name_plural = 'Часто задаваемые вопросы'


class Contact(StaticPage):
    image = models.ImageField(
        upload_to=generate_filename, verbose_name='Изображение'
    )
    telegram = models.CharField(
        max_length=255, blank=True, null=True, verbose_name='Телеграм',
    )
    facebook = models.CharField(
        max_length=255, blank=True, null=True, verbose_name='Фейсбук'
    )
    instagram = models.CharField(
        max_length=255, blank=True, null=True, verbose_name='Инстаграм'
    )
    address = models.CharField(
        max_length=255, blank=True, null=True, verbose_name='Адрес'
    )

    class Meta:
        verbose_name = 'Странница контакты'


class ContactPhone(models.Model):
    contact = models.ForeignKey(
        to='Contact', on_delete=models.CASCADE, related_name='phone_numbers',
        verbose_name='Филиал'
    )
    phone = models.CharField(max_length=255, verbose_name='Номер телефона')

    class Meta:
        verbose_name = 'Номер телефона'
        verbose_name_plural = 'Номера телефонов'

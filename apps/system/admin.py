from django.contrib import admin

from adminsortable2.admin import SortableAdminMixin
from solo.admin import SingletonModelAdmin
from apps.system.models import (
    PrivacyPolicy, TermsOfUse, FAQ, Contact, ContactPhone
)


@admin.register(PrivacyPolicy)
class PrivacyPolicyAdmin(SingletonModelAdmin):
    pass


@admin.register(TermsOfUse)
class TermsOfUseAdmin(SingletonModelAdmin):
    pass


@admin.register(FAQ)
class FAQAdmin(SortableAdminMixin, admin.ModelAdmin):
    list_display = ('position', 'question', 'answer')
    list_display_links = ('position', 'question', 'answer')
    search_fields = ['question', 'answer']


class ContactPhoneAdmin(admin.TabularInline):
    model = ContactPhone
    extra = 0


@admin.register(Contact)
class ContactAdmin(SingletonModelAdmin):
    inlines = (ContactPhoneAdmin,)

from django.urls import path

from apps.system.views import (
    PrivacyPolicyAPIView, TermsOfUseAPIView, FAQListAPIView, ContactAPIView
)


urlpatterns = [
    path(
        'privacy-policy/', PrivacyPolicyAPIView.as_view(), name='privacy_policy'
    ),
    path(
        'terms-of-use/', TermsOfUseAPIView.as_view(), name='terms_of_use'
    ),
    path(
        'faq/', FAQListAPIView.as_view(), name='faq_list'
    ),
    path(
        'contact/', ContactAPIView.as_view(), name='contact'
    )
]

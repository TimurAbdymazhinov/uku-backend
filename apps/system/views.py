from rest_framework import generics

from apps.system.models import TermsOfUse, PrivacyPolicy, FAQ, Contact
from apps.system.serializers import (
    TermsOfUseSerializer, PrivacyPolicySerializer, FAQSerializer,
    ContactSerializer
)


class TermsOfUseAPIView(generics.RetrieveAPIView):
    """
       API view for TermsOfUse
    """
    queryset = TermsOfUse.objects.all()
    serializer_class = TermsOfUseSerializer

    def get_object(self):
        return self.get_queryset().first()


class PrivacyPolicyAPIView(generics.RetrieveAPIView):
    """
       API view for PrivacyPolicy
    """
    queryset = PrivacyPolicy.objects.all()
    serializer_class = PrivacyPolicySerializer

    def get_object(self):
        return self.get_queryset().first()


class FAQListAPIView(generics.ListAPIView):
    """
        Api view for get all FAQ list page by 10
    """
    serializer_class = FAQSerializer
    queryset = FAQ.objects.all()


class ContactAPIView(generics.RetrieveAPIView):
    """
       API view for Contact
    """
    queryset = Contact.objects.all().prefetch_related('phone_numbers')
    serializer_class = ContactSerializer

    def get_object(self):
        return self.get_queryset().first()

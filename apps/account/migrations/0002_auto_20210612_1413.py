# Generated by Django 2.2.16 on 2021-06-12 08:13

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('publication', '0002_publicationimage'),
        ('location', '0002_auto_20210604_1443'),
        ('account', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='favorites',
            field=models.ManyToManyField(related_name='users', to='publication.Publication'),
        ),
        migrations.AddField(
            model_name='user',
            name='following',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='followers', to=settings.AUTH_USER_MODEL, verbose_name='Подписчики'),
        ),
        migrations.AddField(
            model_name='user',
            name='instagram',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name='Инстаграм'),
        ),
        migrations.AddField(
            model_name='user',
            name='region',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='users', to='location.Location', verbose_name='Регион'),
        ),
        migrations.AddField(
            model_name='user',
            name='telegram',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name='Телеграм'),
        ),
        migrations.AddField(
            model_name='user',
            name='whatsapp',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name='WhatsApp'),
        ),
    ]

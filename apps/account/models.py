from django.contrib.auth.models import AbstractUser, UserManager
from django.db import models

from apps.location.models import Location
from common.constants import GENDER_TYPE, MALE
from common.utils import generate_filename


class CustomUserManager(UserManager):
    def _create_user(self, phone, password, **extra_fields):
        """
        Create and save a user with the given phone, and password.
        """
        if not phone:
            raise ValueError('The given username must be set')
        user = self.model(phone=phone, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, phone, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(phone, password, **extra_fields)

    def create_superuser(self, phone, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(phone, password, **extra_fields)


class User(AbstractUser):
    username = None
    phone = models.CharField(max_length=25, unique=True, verbose_name='Телефон')
    avatar = models.ImageField(
        upload_to=generate_filename, null=True, blank=True,
        verbose_name='Аватар',
    )

    birth_date = models.DateField(null=True, verbose_name='Дата рождения',)
    gender = models.CharField(
        choices=GENDER_TYPE, default=MALE, max_length=10, verbose_name='Пол',
    )
    tmp_phone = models.CharField(
        max_length=25, verbose_name='Временный телефон', null=True, blank=True,
    )
    is_registration_finish = models.BooleanField(
        default=False, verbose_name='is_registration_finish',
    )
    confirmation_code = models.CharField(
        verbose_name='confirmation code', max_length=6, null=True, blank=True,
    )
    confirmation_date = models.DateTimeField(
        verbose_name='confirmation date', null=True, blank=True,
    )
    is_old_phone_confirmed = models.BooleanField(
        verbose_name='is old phone confirmed', default=False,
    )
    region = models.ForeignKey(
        to=Location, on_delete=models.SET_NULL,
        verbose_name='Регион', related_name='users', null=True, blank=True
    )
    following = models.ManyToManyField(
        to='self', verbose_name='Подписчики',
        related_name='followers'
    )
    telegram = models.CharField(
        verbose_name='Телеграм', max_length=50, null=True, blank=True,
    )
    instagram = models.CharField(
        verbose_name='Инстаграм', max_length=50, null=True, blank=True,
    )
    whatsapp = models.CharField(
        verbose_name='WhatsApp', max_length=50, null=True, blank=True,
    )
    favorites = models.ManyToManyField(
        'publication.Publication', related_name='users'
    )

    objects = CustomUserManager()

    USERNAME_FIELD = 'phone'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'

    def __str__(self):
        return f'{self.phone} {self.first_name}'

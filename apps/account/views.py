from drf_yasg.utils import swagger_auto_schema
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from apps.account.custom_openapi import (
    AuthRetrieveAPIView, AuthUpdateAPIView, auth_param,
)
from apps.account.paginations import (
    ProfilePublicationPagination, FeedPublicationPagination
)
from apps.account.serializers import (
    PhoneAuthSerializer, LoginConfirmationCodeSerializer, UserUpdateSerializer,
    LoginConfirmAPIViewResponseSerializer, UserAvatarUpdateSerializer,
    AuthErrorSerializer, ConfirmationCodeSerializer, ChageOldPhoneSerializer,
    UserProfileSerializer, UserSearchListSerializer
)
from apps.account.service import (
    UserAuthService, PhoneConfirmationService, ChangeOldPhoneService,
    UserService
)
from apps.publication.models import Publication
from apps.publication.serializers import (
    ProfilePublicationListSerializer, PublicationListSerializer
)
from common.custom_openapi import q


class AuthAPIView(generics.GenericAPIView):
    """ Эндпоинт для login или создания пользователя и отсылки SMS """
    serializer_class = PhoneAuthSerializer

    @swagger_auto_schema(
        responses={
            200: '{"message": "Сообщение отправлено"}',
            201: '{"message": "User создан! Сообщение отправлено"}',
            400: "It will return error type",
            429: '{"message": "Вы слишком часто отправляете сообщение."}',
        }
    )
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        return UserAuthService.get_response(serializer)


class LoginConfirmAPIView(generics.GenericAPIView):
    """ Endpoint для подтверждения номера и авторизации пользователя """
    serializer_class = LoginConfirmationCodeSerializer

    @swagger_auto_schema(
        responses={
            200: LoginConfirmAPIViewResponseSerializer(),
            400: 'It will return error type',
            403: '{"message": "Неверный код"}',
            404: '{"detail": "user not found"}',
        }
    )
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        return PhoneConfirmationService.get_response(serializer)


class SendSmsToOldPhoneAPIView(generics.GenericAPIView):
    """ Endpoint for send sms to old phone number """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(
        manual_parameters=[auth_param],
        responses={
            200: '{"message": "Сообщение отправлено"}',
            400: "It will return error type",
            401: AuthErrorSerializer(),
            429: '{"message": "Вы слишком часто отправляете сообщение."}',
        }
    )
    def get(self, request, *args, **kwargs):
        user = request.user
        return UserAuthService.send_to_old_phone(user)


class OldPhoneConfirmAPIView(generics.GenericAPIView):
    """ Endpoint для подтверждения old phone number """
    permission_classes = (IsAuthenticated,)
    serializer_class = ConfirmationCodeSerializer

    @swagger_auto_schema(
        manual_parameters=[auth_param],
        responses={
            200: '{"message": "Old phone is confirmed"}',
            400: 'It will return error type',
            401: AuthErrorSerializer(),
            403: '{"message": "Неверный код"}',
        }
    )
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = request.user

        return PhoneConfirmationService.get_response_for_old_phone_confirmation(
            user, serializer
        )


class ChangeOldPhoneAPIView(generics.GenericAPIView):
    """ Endpoint для смены old phone number """
    permission_classes = (IsAuthenticated,)
    serializer_class = ChageOldPhoneSerializer

    @swagger_auto_schema(
        responses={
            200: '{"message": "Сообщение отправлено"}',
            400: "It will return error type",
            406: "{'message': 'Такой номер телефона уже существует'}",
            429: '{"message": "Вы слишком часто отправляете сообщение."}',
        }
    )
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(
            data=request.data, context={'request': request}
        )
        serializer.is_valid(raise_exception=True)

        return ChangeOldPhoneService.get_response(serializer)


class NewPhoneConfirmAPIView(generics.GenericAPIView):
    """ Endpoint для подтверждения new phone number """
    permission_classes = (IsAuthenticated,)
    serializer_class = ConfirmationCodeSerializer

    @swagger_auto_schema(
        manual_parameters=[auth_param],
        responses={
            200: '{"message": "New phone is confirmed"}',
            400: 'It will return error type',
            401: AuthErrorSerializer(),
            403: '{"message": "Неверный код"}',
        }
    )
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = request.user
        return ChangeOldPhoneService.get_response_for_new_phone_confirmation(
            user, serializer
        )


class UserUpdateAPIView(AuthUpdateAPIView):
    """Endpoint for update user"""
    http_method_names = ('patch',)
    serializer_class = UserUpdateSerializer

    def get_object(self):
        return self.request.user


class UserAvatarRetrieveUpdateAPIView(AuthRetrieveAPIView, AuthUpdateAPIView):
    """Endpoint for update user image"""
    http_method_names = ('patch', 'get')
    serializer_class = UserAvatarUpdateSerializer

    def get_object(self):
        return self.request.user


class UserProfileRetrieveAPIView(AuthRetrieveAPIView):
    """Endpoint for user profile"""
    serializer_class = UserProfileSerializer

    def get_object(self):
        return self.request.user


class UserPublicationAPIView(generics.ListAPIView):
    """Endpoint for user publications"""
    serializer_class = ProfilePublicationListSerializer
    pagination_class = ProfilePublicationPagination

    def get_queryset(self):
        return (
            Publication.objects
                .filter(user=self.request.user)
                .select_related('category')
        )


class UserFeedAPIView(generics.ListAPIView):
    """Endpoint for user feed"""
    serializer_class = PublicationListSerializer
    pagination_class = FeedPublicationPagination
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return (
            UserService.get_user_feed_publication_queryset(
                self.request.user
            )
        )


class UserSearchAPIView(generics.GenericAPIView):
    """Endpoint for user search api"""
    serializer_class = UserSearchListSerializer

    @swagger_auto_schema(
        manual_parameters=[q],
    )
    def get(self, *args, **kwargs):
        query_string = self.request.query_params.get('q')
        users = UserService.get_user_list_by_q(query_string)
        serializer = self.serializer_class(users, many=True)

        return Response(serializer.data)


class UserFavoriteAPIView(generics.ListAPIView):
    """Endpoint for user favorite publication"""
    serializer_class = PublicationListSerializer
    pagination_class = FeedPublicationPagination
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return (
            UserService.get_user_favorite_publication_queryset(
                self.request.user
            )
        )

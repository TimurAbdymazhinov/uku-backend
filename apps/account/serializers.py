from drf_yasg.utils import swagger_serializer_method
from rest_framework import serializers

from apps.account.models import User
from apps.account.service import (
    UserAuthService, PhoneConfirmationService, ChangeOldPhoneService,
)



class PhoneAuthSerializer(serializers.Serializer):
    """Сериалайзер для валидации и создания пользователя """
    phone = serializers.CharField(required=True)

    def validate(self, data):
        self.user, self.created = (
            UserAuthService.get_or_create_user_instance(phone_number=data.get('phone'))
        )
        if not self.created:
            self.confirm_login_allowed(self.user)
        return data

    def validate_phone(self, value):
        UserAuthService.check_format_user_phone(value)
        return value

    @staticmethod
    def confirm_login_allowed(user):
        if not user.is_active:
            raise serializers.ValidationError({'phone': 'Этот номер не активен.'})


class ConfirmationCodeSerializer(serializers.Serializer):
    """Serializer for phone code confirmation"""
    confirmation_code = serializers.CharField(max_length=6, required=True)


class LoginConfirmationCodeSerializer(PhoneAuthSerializer, ConfirmationCodeSerializer):
    """ Сериалайзер для login код подтверждения """

    def validate(self, data):
        PhoneConfirmationService.check_is_user_exists(data.get('phone'))
        self.confirm_login_allowed(data.get('phone'))
        return data

    def validate_phone(self, value):
        super(LoginConfirmationCodeSerializer, self).validate_phone(value)
        PhoneConfirmationService.check_is_user_exists(value)
        return value

    @staticmethod
    def confirm_login_allowed(phone: str) -> None:
        if not User.objects.filter(phone=phone, is_active=True).exists():
            raise serializers.ValidationError({'phone': 'Этот номер не активен.'})


class ChageOldPhoneSerializer(PhoneAuthSerializer):
    """ Сериалайзер для login код подтверждения """

    def validate(self, data):
        request = self.context['request']
        self.user = (
            ChangeOldPhoneService.set_tmp_phone_number(
                phone_number=data.get('phone'),
                user=request.user,
            )
        )
        return data

    def validate_phone(self, value):
        return ChangeOldPhoneService.check_format_user_phone(value)


class LoginConfirmAPIViewResponseSerializer(serializers.Serializer):
    token = serializers.CharField()
    is_profile_completed = serializers.BooleanField()


class UserUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'gender', 'birth_date']
        extra_kwargs = {
            field: {'required': True} for field in fields
        }


class UserAvatarUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['avatar']
        extra_kwargs = {'avatar': {'required': True}}


class AuthErrorSerializer(serializers.Serializer):
    """ Error response on status.HTTP_401_UNAUTHORIZED """
    detail = serializers.CharField(help_text='This is error text')


class UserPublicationListSerializer(serializers.ModelSerializer):
    location = serializers.CharField(source='region.name', read_only=True)

    class Meta:
        model = User
        fields = ['id', 'first_name', 'last_name', 'avatar', 'location']


class UserProfileSerializer(serializers.ModelSerializer):
    followers_count = serializers.SerializerMethodField()
    following_count = serializers.SerializerMethodField()
    publications_count = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = [
            'id', 'first_name', 'last_name', 'avatar',
            'followers_count', 'following_count', 'publications_count',
            'instagram', 'telegram', 'whatsapp',
        ]

    def get_following_count(self, obj):
        if obj.following:
            return obj.following.count()

        return 0

    def get_followers_count(self, obj):
        if hasattr(obj, 'followers'):
            return obj.followers.count()
        return 0

    def get_publications_count(self, obj):
        if hasattr(obj, 'publications'):
            return obj.publications.count()
        return 0


class UserSearchListSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'first_name', 'last_name', 'avatar', 'phone']

from rest_framework.response import Response
from rest_framework.pagination import PageNumberPagination


class ListPagination(PageNumberPagination):
    page_size = 20

    def get_paginated_response(self, data):
        return Response({
            'count': self.page.paginator.count,
            'next': self.page.next_page_number() if self.page.has_next() else None,
            'previous': self.page.previous_page_number() if self.page.has_previous() else None,
            'results': data,
        })


class ProfilePublicationPagination(ListPagination):
    page_size = 9


class FeedPublicationPagination(ListPagination):
    page_size = 12

from rest_framework import serializers

from apps.account.serializers import UserPublicationListSerializer
from apps.category.serializers import CategorySerializer
from apps.location.serializers import LocationSerializer
from apps.publication.models import Publication, PublicationImage


class PublicationListImageSerializer(serializers.ModelSerializer):
    image = serializers.SerializerMethodField()

    class Meta:
        model = PublicationImage
        fields = ['image', 'is_main']

    def get_image(self, obj):
        if obj.image_small:
            request = self.context['request']
            return request.build_absolute_uri(obj.image_small.url)

        return None


class PublicationListSerializer(serializers.ModelSerializer):
    user = UserPublicationListSerializer()
    categories = serializers.SerializerMethodField()
    images = PublicationListImageSerializer(many=True)

    class Meta:
        model = Publication
        fields = [
            'id', 'categories', 'user', 'description', 'created_at',
            'viewed', 'images'
        ]

    def get_categories(self, obj):
        serializer = CategorySerializer(obj.category.get_family(), many=True)
        data = serializer.data
        return data


class ProfilePublicationListSerializer(serializers.ModelSerializer):
    categories = serializers.SerializerMethodField()
    images = PublicationListImageSerializer(many=True)

    class Meta:
        model = Publication
        fields = [
            'id', 'categories', 'user', 'description', 'created_at',
            'viewed', 'images'
        ]

    def get_categories(self, obj):
        serializer = CategorySerializer(obj.category.get_family(), many=True)
        data = serializer.data
        return data

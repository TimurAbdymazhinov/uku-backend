from django.apps import AppConfig


class PublicationConfig(AppConfig):
    name = 'apps.publication'
    verbose_name = 'Публикация'

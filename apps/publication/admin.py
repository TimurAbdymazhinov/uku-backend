from django.contrib import admin

from apps.publication.models import Publication, PublicationImage


class PublicationImageAdmin(admin.TabularInline):
    model = PublicationImage
    extra = 0


@admin.register(Publication)
class PublicationAdmin(admin.ModelAdmin):
    inlines = [PublicationImageAdmin]

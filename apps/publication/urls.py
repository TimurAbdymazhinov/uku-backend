from django.urls import path

from apps.publication.views import (
    PublicationListAPIView
)

urlpatterns = [
    path(
        'category/<int:id>', PublicationListAPIView.as_view(),
        name='get_all_publication'
    ),
]

from django.db.models import Q

from apps.publication.models import Publication


class PublicationService:

    @classmethod
    def get_publication_queryset(cls, category_list, location):
        filter_params = []

        if category_list:
            filter_params.append(Q(category__in=category_list))

        if location:
            filter_params.append(Q(location=location))

        queryset = (
            Publication.objects
            .filter(*filter_params)
            .select_related('user')
            .select_related('location')
            .prefetch_related('images')
        )

        return queryset

from drf_yasg.utils import swagger_auto_schema
from rest_framework import generics

from apps.category.services import CategoryService
from apps.location.services import LocationService
from apps.publication.serializers import PublicationListSerializer
from apps.publication.services import PublicationService
from common.custom_openapi import location_id


class PublicationListAPIView(generics.ListAPIView):
    """Publication all list API View"""
    serializer_class = PublicationListSerializer

    def get_queryset(self):
        category_id = self.kwargs.get('id')
        location_id = self.request.query_params.get('location')

        category_list = CategoryService.get_all_children_category_id_by_list(
            category_id
        )
        location = LocationService.get_location_by_id(location_id)

        return PublicationService.get_publication_queryset(
            category_list, location
        )

    @swagger_auto_schema(
        manual_parameters=[location_id],
    )
    def get(self, request, *args, **kwargs):
        return super(PublicationListAPIView, self).get(request, *args, **kwargs)

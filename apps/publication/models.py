from django.db import models

from mptt.fields import TreeForeignKey

from imagekit.models import ImageSpecField
from pilkit.processors import ResizeToFill

from apps.account.models import User
from apps.category.models import Category
from apps.location.models import Location
from common.utils import generate_filename


class Publication(models.Model):
    class Meta:
        verbose_name = 'Публикация'
        verbose_name_plural = 'Публикации'

    category = TreeForeignKey(
        to=Category, on_delete=models.SET_NULL, null=True,
        verbose_name='Категория', related_name='publications'
    )
    user = models.ForeignKey(
        to=User, on_delete=models.CASCADE,
        verbose_name='Пользователь', related_name='publications'
    )
    location = models.ForeignKey(
        to=Location, on_delete=models.SET_NULL, null=True,
        verbose_name='Локация', related_name='publications'
    )

    description = models.TextField(
        verbose_name='Описание', null=True, blank=True
    )
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='Дата создания'
    )
    viewed = models.BigIntegerField(default=0, verbose_name='Просмотрено')


class PublicationImage(models.Model):
    publication = models.ForeignKey(
        to=Publication, on_delete=models.CASCADE,
        verbose_name='Публикация', related_name='images'
    )
    image = models.ImageField(
        upload_to=generate_filename, verbose_name='Изображение'
    )
    image_small = ImageSpecField(
        source='image', processors=[ResizeToFill(285, 190)], format='JPEG',
        options={'quality': 100},
    )
    is_main = models.BooleanField(default=False, verbose_name='Главная?')

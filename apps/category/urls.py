from django.urls import path

from apps.category.views import (
    CategoryTypeListAPIView
)

urlpatterns = [
    path('', CategoryTypeListAPIView.as_view(), name='get_all_category'),
]

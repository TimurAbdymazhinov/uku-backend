from rest_framework import serializers
from rest_framework_recursive.fields import RecursiveField

from apps.category.models import Category, CategoryType


class CategoryTreeSerializer(serializers.ModelSerializer):
    children = RecursiveField(many=True)

    class Meta:
        model = Category
        fields = ['id', 'name', 'children']


class CategoryTypeSerializer(serializers.ModelSerializer):
    categories = CategoryTreeSerializer(many=True)

    class Meta:
        model = CategoryType
        fields = ['id', 'name', 'categories']


class CategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = ['id', 'name']

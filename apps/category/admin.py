from django.contrib import admin

from mptt.admin import MPTTModelAdmin

from apps.category.models import Category, CategoryType


@admin.register(CategoryType)
class CategoryTypeAdmin(admin.ModelAdmin):
    list_display = ('name',)
    list_display_links = ('name',)


@admin.register(Category)
class CategoryAdmin(MPTTModelAdmin):
    mptt_indent_field = "name"
    list_display = ('name', 'category_type')
    list_display_links = ('name',)
    prepopulated_fields = {'slug': ('name',)}

    fieldsets = [
        [None, {'fields': ['name', 'slug', 'parent', 'category_type']}],
    ]

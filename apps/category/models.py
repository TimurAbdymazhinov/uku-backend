from django.db import models

from apps.location.models import TreeModel


class CategoryType(models.Model):
    name = models.CharField(max_length=255, verbose_name='Название')

    class Meta:
        verbose_name = 'Тип категории'
        verbose_name_plural = 'Типы категории'

    def __str__(self):
        return self.name


class Category(TreeModel):
    category_type = models.ForeignKey(
        CategoryType, on_delete=models.CASCADE, null=True, blank=True,
        verbose_name='Тип категории', related_name='categories'
    )

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    def clean(self):
        if self.parent:
            self.category_type = None

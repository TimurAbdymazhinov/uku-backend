from rest_framework import generics

from apps.category.serializers import CategoryTypeSerializer
from apps.category.models import CategoryType


class CategoryTypeListAPIView(generics.ListAPIView):
    """Category Type list API View"""
    serializer_class = CategoryTypeSerializer
    queryset = CategoryType.objects.all().prefetch_related('categories')

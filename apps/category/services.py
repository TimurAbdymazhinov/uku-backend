from apps.category.models import Category


class CategoryService:

    @classmethod
    def get_all_children_category_id_by_list(cls, category_id):
        category = cls.get_category(category_id)
        category_list = []
        if category:
            category_list = category.get_descendants(include_self=True)

        return category_list

    @staticmethod
    def get_category(category_id):
        category = None
        try:
            category = Category.objects.get(id=category_id)
        except Category.DoesNotExist:
            pass

        return category




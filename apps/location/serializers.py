from rest_framework import serializers
from rest_framework_recursive.fields import RecursiveField

from apps.location.models import Location


class LocationTreeSerializer(serializers.ModelSerializer):
    children = RecursiveField(many=True)

    class Meta:
        model = Location
        fields = ['id', 'name', 'children']


class LocationSerializer(serializers.ModelSerializer):

    class Meta:
        model = Location
        fields = ['id', 'name']

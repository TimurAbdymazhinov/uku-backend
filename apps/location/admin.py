from django.contrib import admin

from mptt.admin import MPTTModelAdmin

from apps.location.models import Location


@admin.register(Location)
class LocationAdmin(MPTTModelAdmin):
    mptt_indent_field = "name"
    list_display = ('name',)
    list_display_links = ('name',)
    prepopulated_fields = {'slug': ('name',)}

    fieldsets = [
        [None, {'fields': ['name', 'slug', 'parent']}],
    ]

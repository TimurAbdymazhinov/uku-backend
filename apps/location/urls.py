from django.urls import path

from apps.location.views import (
    LocationListAPIView
)

urlpatterns = [
    path('', LocationListAPIView.as_view(), name='get_all_location'),
]

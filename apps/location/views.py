from rest_framework import generics

from apps.location.serializers import LocationTreeSerializer
from apps.location.models import Location


class LocationListAPIView(generics.ListAPIView):
    """Location list API View"""
    serializer_class = LocationTreeSerializer
    queryset = Location.objects.filter(parent=None)

from django.db import models

from mptt.models import MPTTModel, TreeForeignKey


class TreeModel(MPTTModel):
    name = models.CharField(max_length=255, verbose_name='Название')
    slug = models.CharField(max_length=255, verbose_name='SLUG', unique=True)
    parent = TreeForeignKey(
        'self', on_delete=models.SET_NULL, null=True, blank=True,
        verbose_name='Родительская', related_name='children'
    )

    def __str__(self):
        return self.name

    class Meta:
        abstract = True


class Location(TreeModel):
    class Meta:
        verbose_name = 'Регион'
        verbose_name_plural = 'Регионы'

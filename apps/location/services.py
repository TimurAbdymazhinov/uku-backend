from apps.location.models import Location


class LocationService:

    @classmethod
    def get_location_by_id(cls, location_id):
        location = None
        try:
            location = Location.objects.get(id=location_id)
        except Location.DoesNotExist:
            pass

        return location

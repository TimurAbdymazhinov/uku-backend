from drf_yasg import openapi

location_id = openapi.Parameter(
    'location', openapi.IN_QUERY,
    description="Query param LOCATION", type=openapi.TYPE_NUMBER
)

q = openapi.Parameter(
    'q', openapi.IN_QUERY,
    description="Query param q", type=openapi.TYPE_STRING
)

MALE = 'male'
FEMALE = 'female'

GENDER_TYPE = (
    (MALE, 'Мужской'),
    (FEMALE, 'Женский')
)
